#!/usr/bin/env python3
# GPL License, Version 3.0 or later

import shutil
import unittest
import unittest.mock

import os
import sys

VERBOSE = os.environ.get('VERBOSE', False)
TEMP_LOCAL = '/tmp/ustow'

BASE_DIR = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))

USTOW_DIR = '_ustow'
USTOW_CHECKSUMS = 'sha512sum.txt'


def file_create_with_data(data, *paths):
    filepath = os.path.join(*paths)
    dirname = os.path.dirname(filepath)

    os.makedirs(dirname, exist_ok=True)

    with open(filepath, 'wb') as fh:
        fh.write(data)


def file_read_data(*paths):
    filepath = os.path.join(*paths)
    with open(filepath, 'rb') as fh:
        return fh.read()


class StdIO:
    __slots__ = (
        'stdout',
        'stderr',
    )

    def __init__(self):
        self.stdout = sys.stdout
        self.stderr = sys.stderr

    def read(self):
        sys.stdout.seek(0)
        sys.stderr.seek(0)
        return sys.stdout.read(), sys.stderr.read()

    def __enter__(self):
        import io
        sys.stdout = io.StringIO()
        sys.stderr = io.StringIO()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            self.stdout.write('\n'.join(self.read()))

        sys.stdout = self.stdout
        sys.stderr = self.stderr


def execfile(filepath, *, mod=None, name='__main__'):
    # module name isn't used or added to 'sys.modules'.
    # passing in 'mod' allows re-execution without having to reload.

    import importlib.util
    import importlib.machinery
    loader = importlib.machinery.SourceFileLoader(name, filepath)

    mod_spec = importlib.util.spec_from_file_location(name, filepath, loader=loader)

    if mod is None:
        mod = importlib.util.module_from_spec(mod_spec)
    mod_spec.loader.exec_module(mod)
    return mod


def ustow_run(argv):
    ustow = execfile(os.path.join(BASE_DIR, 'ustow'), name='ustow')

    if VERBOSE:
        sys.stdout.write('\n  running:  ')
        import shlex
        sys.stdout.write('ustow %s\n' % ' '.join([shlex.quote(c) for c in argv]))

    with StdIO() as fakeio:
        ustow.main(argv)
        ret = fakeio.read()

    if VERBOSE:
        sys.stdout.write('   stdout:  %s\n' % ret[0].strip())
        sys.stdout.write('   stderr:  %s\n' % ret[1].strip())

    return ret


class UStowSessionTestCase(unittest.TestCase):

    def assertFileExists(self, filepath, msg=None):
        if not os.path.exists(filepath):
            if msg is None:
                msg = 'File %r does not exist' % os.path.relpath(filepath, TEMP_LOCAL)
            raise self.failureException(msg)

    def setUp(self):

        # for running single tests
        if __name__ != '__main__':
            self._data = global_setup()

        if not os.path.isdir(TEMP_LOCAL):
            os.makedirs(TEMP_LOCAL)

    def tearDown(self):
        shutil.rmtree(TEMP_LOCAL)

        # for running single tests
        if __name__ != '__main__':
            global_teardown(self._data)

    def init_defaults(self, *, paths=None):
        if paths is None:
            paths = ('a', 'b', 'c')

        self.base_paths = tuple(paths)

    def get_base_paths(self):
        return [
            os.path.join(TEMP_LOCAL, p)
            for p in self.base_paths
        ]

    def init_repo(self, *, base_paths=None):
        base_paths = self.get_base_paths()
        for p in base_paths:
            os.makedirs(p)

        stdout, stderr = ustow_run(['init', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertEqual('', stdout)
        return base_paths


class UStowInitTest(UStowSessionTestCase):
    '''
    Test the `ustow init paths...` command.
    We verify that a project directory is created, and that it contains a '.ustow' sub-directory.
    '''

    def __init__(self, *args):
        self.init_defaults()
        super().__init__(*args)

    def test_init(self):
        base_paths = self.init_repo()
        for p in base_paths:
            filepath = os.path.join(p, USTOW_DIR, USTOW_CHECKSUMS)
            self.assertFileExists(filepath)


class UStowSyncTest(UStowSessionTestCase):
    '''
    Test the `ustow init paths...` command.
    We verify that a project directory is created, and that it contains a '.ustow' sub-directory.
    '''

    def __init__(self, *args):
        self.init_defaults()
        super().__init__(*args)

    def test_sync_none(self):
        base_paths = self.init_repo()

        stdout, stderr = ustow_run(['sync', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\nSync copying 0 file\(s\)\n')

    def test_sync_simple_file(self):
        base_paths = self.init_repo()

        filename = 'test.dat'
        test_data = b'filedata'
        file_create_with_data(test_data, base_paths[0], filename)
        stdout, stderr = ustow_run(['sync', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\nSync copying 2 file\(s\)\n')

        for i in range(1, len(base_paths)):
            filepath = os.path.join(base_paths[i], filename)
            self.assertFileExists(filepath)
            self.assertEqual(test_data, file_read_data(filepath))

    def test_sync_tree_of_file(self):
        base_paths = self.init_repo()
        files = [
            'test_1.dat',
            os.path.join('subdir', 'test_2.dat'),
            os.path.join('subdir', 'subsubdir', 'test_3.dat'),
        ]
        for filename in files:
            test_data = filename.upper().encode()
            file_create_with_data(test_data, base_paths[0], filename)
        stdout, stderr = ustow_run(['sync', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\nSync copying 6 file\(s\)\n')

        for i in range(len(base_paths)):
            for filename in files:
                filepath = os.path.join(base_paths[i], filename)
                test_data = filename.upper().encode()
                self.assertFileExists(filepath)
                self.assertEqual(test_data, file_read_data(filepath))


class UStowCheckTest(UStowSessionTestCase):
    '''
    Test the `ustow check paths...` command.
    '''

    def __init__(self, *args):
        self.init_defaults()
        super().__init__(*args)

    def test_sync_none(self):
        base_paths = self.init_repo()

        stdout, stderr = ustow_run(['sync', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\nSync copying 0 file\(s\)\n')

    def test_check_none(self):
        base_paths = self.init_repo()
        stdout, stderr = ustow_run(['check', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\nNo errors detected!')

    def test_check_simple(self):
        base_paths = self.init_repo()

        filename = 'test.dat'
        test_data = b'filedata'
        file_create_with_data(test_data, base_paths[0], filename)
        stdout, stderr = ustow_run(['sync', '--base-paths', *base_paths])
        self.assertEqual('', stderr)

        stdout, stderr = ustow_run(['check', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\nChecking 3 file\(s\)\n')
        # import time
        # time.sleep(100000)
        self.assertRegex(stdout, r'\nNo errors detected!')

        file_create_with_data(test_data + b'_', base_paths[0], filename)

        stdout, stderr = ustow_run(['check', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\[CHECKSUM_MISMATCH: 1\]')


class UStowPruneTest(UStowSessionTestCase):
    '''
    Test the `ustow prune paths...` command.
    '''

    def __init__(self, *args):
        self.init_defaults()
        super().__init__(*args)

    def test_prune_none(self):
        base_paths = self.init_repo()

        stdout, stderr = ustow_run(['prune', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\nPruning 0 file\(s\)\n')

    def test_prune_tree_of_files(self):
        base_paths = self.init_repo()
        files = [
            'test_1.dat',
            os.path.join('subdir', 'test_2.dat'),
            os.path.join('subdir', 'subsubdir', 'test_3.dat'),
        ]
        for filename in files:
            test_data = filename.upper().encode()
            file_create_with_data(test_data, base_paths[0], filename)
        stdout, stderr = ustow_run(['sync', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\nSync copying 6 file\(s\)\n')

        # Remove all files from one of the paths.
        for filename in files:
            os.remove(os.path.join(base_paths[0], filename))

        # Prune the rest.
        stdout, stderr = ustow_run(['prune', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\nPruning 6 file\(s\)\n')

        stdout, stderr = ustow_run(['check', '--base-paths', *base_paths])
        self.assertEqual('', stderr)
        self.assertRegex(stdout, r'\nChecking 0 file\(s\)\n')
        self.assertRegex(stdout, r'\nNo errors detected!')


def global_setup():
    data = None
    shutil.rmtree(TEMP_LOCAL, ignore_errors=True)
    return data


def global_teardown(_data):

    shutil.rmtree(TEMP_LOCAL, ignore_errors=True)


if __name__ == '__main__':
    data = global_setup()
    unittest.main(exit=False)
    global_teardown(data)
