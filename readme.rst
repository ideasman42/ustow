
##########
Micro-Stow
##########

Automate syncing multiple copies of files between drives, with checksum validation.


Motivation
==========

While manually copying files between paths and validating check-sums isn't especially hard, it's tedious.

This utility is intended to make this simple enough not to be a chore.


Usage
=====

To begin with, initialize the paths you want to mirror::

   ustore init --base-paths /path/a /path/b /path/c

After this, running sync will copy files between all drives, creating files on devices which don't contain them::

   ustore sync --base-paths /path/a /path/b /path/c

To ensure no corruption has occurred, periodically validate the check-sums::

   ustore check --base-paths /path/a /path/b /path/c

If you wish to remove files, first remove them from a single device, then run::

   ustore prune --dry-run --base-paths /path/a /path/b /path/c

If the list of files is acceptable, run::

   ustore prune --base-paths /path/a /path/b /path/c


Running Tests
=============

Tests can be run simply by calling::

   python3 tests/test_cli.py

You may wish to run the test on save, for example::

   bash -c 'while true; do inotifywait -e close_write ./ustow ./tests/test_cli.py; sleep 0.3; clear; python tests/test_cli.py; done'



TODO
====

- Sync should use the newest files when they don't match.
- Investigate properly supporting case insensitive file-systems,
  where the cases on file-systems don't match.
- Support syncing multiple corrupted paths to a new target.


Help Text
=========

.. BEGIN HELP TEXT

Output of ``ustow --help``

usage::

       ustow [-h] {init,check,sync,prune} ...

micro-stow, local redundant backups, with checksum validation.

positional arguments:
  {init,check,sync,prune}
    init                Initialize paths.
    check               Report any checksum mismatches.
    sync                Ensure all devices contain a full set of files.
    prune               Prune files missing from any of the drives.

optional arguments:
  -h, --help            show this help message and exit

typically this should be executed by a wrapper or shell alias.

Examples:

  ustow init --base-paths /path/a /path/b
  ustow sync --base-paths /path/a /path/b

Sub-Command: ``init``
---------------------

usage::

       ustow init [-h] -p BASE_PATHS [BASE_PATHS ...]

This creates the directory used to store internal data, so other commands such as sync can be performed.


optional arguments:
  -h, --help            show this help message and exit
  -p BASE_PATHS [BASE_PATHS ...], --base-paths BASE_PATHS [BASE_PATHS ...]
                        Path(s) to backup to

Sub-Command: ``check``
----------------------

usage::

       ustow check [-h] -p BASE_PATHS [BASE_PATHS ...]

All files are scanned and compared against the internal check-sums.

A summary is reported at the end, to report corrupt files on any of the devices.


optional arguments:
  -h, --help            show this help message and exit
  -p BASE_PATHS [BASE_PATHS ...], --base-paths BASE_PATHS [BASE_PATHS ...]
                        Path(s) to backup to

Sub-Command: ``sync``
---------------------

usage::

       ustow sync [-h] -p BASE_PATHS [BASE_PATHS ...]

This command updates check-sums, then copies files onto other devices as needed.


optional arguments:
  -h, --help            show this help message and exit
  -p BASE_PATHS [BASE_PATHS ...], --base-paths BASE_PATHS [BASE_PATHS ...]
                        Path(s) to backup to

Sub-Command: ``prune``
----------------------

usage::

       ustow prune [-h] -p BASE_PATHS [BASE_PATHS ...] [-d]

It is recommended to use '--dry-run' first in most case.


optional arguments:
  -h, --help            show this help message and exit
  -p BASE_PATHS [BASE_PATHS ...], --base-paths BASE_PATHS [BASE_PATHS ...]
                        Path(s) to backup to
  -d, --dry-run         Don perform changes

.. END HELP TEXT
